/**
 * mo-line manages display of Molenbeek"s line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


import { Url } from "url";
import { Message } from "stompjs";
import { Maybe, Some, None, List, IO } from "monet";
import { Stream, Subscriber, from } from "most";
import { create as createObservable, Emitter } from "./Observable";
import { createStream } from "./Receiver";
import { StompBody, extractBody, update } from "./Store";
import { create as createSink } from "./Sink";


const consume: (a: Maybe<StompBody>) => void =
    (data) => {
        if (data.isSome()) {
            update(data.some());
        }
    };


const toBody: (msg: Message) => Maybe<StompBody> =
    (msg) => extractBody(msg.body);


const plug = (source: string, destination: string) => {

    const rePlug = () => {
        console.error("Stream Interrupted, Replug");
        setTimeout(() => plug(source, destination), 1000);
    };

    createStream(source, destination)
        .map<Maybe<StompBody>>(toBody)
        .observe(consume)
        .then(rePlug)
        .catch(rePlug);
};



const main: (a: string, b: string, c: string, d: string) => void =
    (source, destination, panelHost, panelPort) => {
        createSink(panelHost, parseInt(panelPort));
        plug(source, destination);
    };


export default main;

"use strict";
/**
 * mo-line manages display of Molenbeek"s line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const program = require("commander");
const Main_1 = require("./Main");
program
    .version("1.0.0")
    .arguments("<orchestra> <destination> <panel_host> <panel_port>")
    .action((orchestra, destination, panelHost, panelPort) => {
    Main_1.default(orchestra, destination, panelHost, panelPort);
})
    .parse(process.argv);
if (program.args.length === 0) {
    program.outputHelp();
    process.exit(1);
}

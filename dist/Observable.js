"use strict";
/**
 * mo-line manages display of Molenbeek"s line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const symbol_observable_1 = require("symbol-observable");
const createSubscriber = function (observer) {
    let alive = true;
    const s = {
        next: (v) => {
            if (alive)
                observer.next(v);
        },
        error: (err) => {
            if (alive)
                observer.error(err);
        },
        complete: () => {
            if (alive)
                observer.complete();
        },
    };
    return {
        subscriber: s,
        subscription: {
            unsubscribe: () => {
                alive = false;
            }
        }
    };
};
const createSubscribe = function (subs) {
    return (observer) => {
        const ss = createSubscriber(observer);
        subs.push(ss.subscriber);
        return ss.subscription;
    };
};
exports.create = function (emitter) {
    return (subs) => {
        // emitter takes over the list of suscribers
        // and we avoid attaching it to a specific instance, the caller being
        // in charge of managing it;
        emitter(subs);
        const observable = () => {
            return {
                [symbol_observable_1.default]: observable,
                subscribe: createSubscribe(subs),
            };
        };
        return observable();
    };
};

"use strict";
/**
 * mo-line manages display of Molenbeek"s line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const debug = require("debug");
const Lingua_1 = require("./Lingua");
const log = debug("mo:message");
const HEAD_SIZE = 9;
const FRAME_SIZE = 4000;
const FONT = 0x00;
const FCOLOR = 0x01;
const BGCOLOR = 0x00;
const ui8 = (buf, idx = 0) => {
    return buf.readUInt8(idx);
};
const ui16 = (buf, idx = 0) => {
    return buf.readUInt16BE(idx);
};
const checksum = (slice) => {
    return slice.reduce((prev, cur) => prev ^ cur, 0);
};
const hi = (n) => {
    return (0x0000ff00 & n) >> 8;
};
const low = (n) => {
    return (0x000000ff & n);
};
const bound = (sz) => (n) => {
    const max = Math.pow(2, sz) - 1;
    return Math.min(max, n);
};
const bound8 = bound(8);
const bound16 = bound(16);
const writeSequence = (buf, seq, offset) => {
    return seq.reduce((memo, c) => buf.writeUInt8(c, memo), offset);
};
exports.createMessage = (messageData) => {
    const buf = Buffer.alloc(FRAME_SIZE - (HEAD_SIZE + 1));
    const header = [
        exports.functionCode(Lingua_1.FC.SendToInitialSegment),
        exports.clearBuffer()
    ];
    const end = [
        exports.commandCode(Lingua_1.CC.EndOfSegementData)
    ];
    const data = header.concat(messageData, end);
    // log("=================================");
    // data.forEach((seq) => {
    //     const seqString = seq.reduce((memo, c) => {
    //         return memo + c.toString(16) + " "
    //     }, "");
    //     log(seqString);
    // });
    const offset = data.reduce((memo, seq) => writeSequence(buf, seq, memo), 0);
    const c = checksum(buf.slice(0, offset));
    const finalOffset = buf.writeUInt8(c, offset);
    return { buffer: buf, length: finalOffset };
};
exports.functionCode = (fc) => {
    return [fc];
};
exports.commandCode = (cc) => {
    return [cc];
};
exports.clearBuffer = () => {
    return [0x01];
};
exports.text = (x, y, str) => {
    const head = [Lingua_1.CC.ShowTextString,
        FONT, FCOLOR, BGCOLOR, x, y];
    const result = Array.from(str)
        .reduce((memo, c) => {
        const i = c.charCodeAt(0);
        if ((i > 0) && (i < 256)) {
            return memo.concat([i]);
        }
        return memo;
    }, head)
        .concat([0x00]);
    return result;
};
exports.delay = (time = 0) => {
    time = bound16(time);
    return [
        Lingua_1.CC.Delay,
        hi(time),
        low(time)
    ];
};
exports.display = (mode = 0, speed = 0x09) => {
    return [
        Lingua_1.CC.DisplayBuffer,
        0x00,
        0x00,
        mode,
        speed
    ];
};
exports.formatMessage = (optSender, optReceiver, message) => {
    const sender = bound16(optSender);
    const receiver = bound16(optReceiver);
    const { buffer, length } = message;
    const buf = Buffer.alloc(HEAD_SIZE + length);
    // protocolId
    buf.writeUInt8(0xaa, 0);
    buf.writeUInt8(0xbb, 1);
    // senderId
    buf.writeUInt8(hi(sender), 2);
    buf.writeUInt8(low(sender), 3);
    // receiverId
    buf.writeUInt8(hi(receiver), 4);
    buf.writeUInt8(low(receiver), 5);
    // message length (witout checksum)
    buf.writeUInt8(hi(length - 1), 6);
    buf.writeUInt8(low(length - 1), 7);
    // header checksum
    buf.writeUInt8(checksum(buf.slice(2, 8)), 8);
    // message.body
    buffer.copy(buf, 9, 0, length);
    return buf;
};
const simpleMessage = (optSender, optReceiver, code) => {
    const buf = Buffer.alloc(2);
    buf.writeUInt8(code, 0);
    buf.writeUInt8(code, 1);
    return exports.formatMessage(optSender, optReceiver, {
        buffer: buf, length: 2
    });
};
exports.ping = (optSender, optReceiver) => {
    return simpleMessage(optSender, optReceiver, Lingua_1.FC.SendPing);
};
exports.clearSegments = (optSender, optReceiver) => {
    return simpleMessage(optSender, optReceiver, Lingua_1.FC.ClearAllSegments);
};
exports.requestResult = (optSender, optReceiver) => {
    return simpleMessage(optSender, optReceiver, Lingua_1.FC.RequestForTransmissionResult);
};
exports.readMessage = (buf) => {
    // sender = bound16(sender);
    // receiver = bound16(receiver);
    //
    // const length = message.offset;
    // let buf = Buffer.alloc(HEAD_SIZE + length);
    // debug('formatMessage:', 'length', length);
    // protocolId
    const pid = ui16(buf, 0);
    // senderId
    const sid = ui16(buf, 2);
    // receiverId
    const rid = ui16(buf, 4);
    // message length (witout checksum)
    const length = ui16(buf, 6);
    // header checksum
    const hcs = ui8(buf, 8);
    // message.body
    const body = buf.slice(9, 9 + length).reduce((acc, item) => {
        return acc.concat(item);
    }, new Array());
    return {
        pid, sid, rid, body
    };
};

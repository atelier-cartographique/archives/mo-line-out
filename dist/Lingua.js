"use strict";
/**
 * mo-line manages display of Molenbeek's line system
 * Copyright (C) 2016  Pierre Marchand <pierremarc07@gmail.com>
 *
 * This program is free software: you can redistribute
 * it and/or modify it under the terms of the GNU Affero
 * General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var FC;
(function (FC) {
    FC[FC["SendToInitialSegment"] = 0] = "SendToInitialSegment";
    FC[FC["SetClock"] = 1] = "SetClock";
    FC[FC["ClearAllSegments"] = 7] = "ClearAllSegments";
    FC[FC["SendPing"] = 32] = "SendPing";
    FC[FC["RequestForTransmissionResult"] = 33] = "RequestForTransmissionResult";
    FC[FC["ResponseForTransmissionResult"] = 49] = "ResponseForTransmissionResult";
})(FC = exports.FC || (exports.FC = {}));
;
var CC;
(function (CC) {
    CC[CC["ClearBuffer"] = 1] = "ClearBuffer";
    CC[CC["ShowTextString"] = 3] = "ShowTextString";
    CC[CC["ShowCurrentTime"] = 4] = "ShowCurrentTime";
    CC[CC["ShowCurrentDate"] = 5] = "ShowCurrentDate";
    CC[CC["Delay"] = 6] = "Delay";
    CC[CC["DisplayBuffer"] = 7] = "DisplayBuffer";
    CC[CC["ShowTextImmediately"] = 8] = "ShowTextImmediately";
    CC[CC["ShowTemperature"] = 11] = "ShowTemperature";
    CC[CC["EndOfSegementData"] = 31] = "EndOfSegementData";
})(CC = exports.CC || (exports.CC = {}));
;
function comName(code) {
    return CC[code];
}
exports.comName = comName;
;
